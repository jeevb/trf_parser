class Node():

    def __init__(self, data):
        self.start, self.end = data

        self.inputs = []
        self.chain = []

    @property
    def score(self):
        return len(self.chain)

    @property
    def _input(self):
        return max(self.inputs, key=lambda n: n.score)

    def add_input(self, node):
        self.inputs.append(node)

    def extend_chain(self):
        if self.inputs:
            self.chain = list(self._input.chain)
        self.chain.append(self)
