class ChainFinder():

    def __init__(self, min_period, max_period):
        self.min_period = min_period
        self.max_period = max_period

    def _find_candidate_chains(self, candidate):
        nodes = candidate.nodes

        for idx, node in enumerate(nodes):
            for i in xrange(idx):
                curr_node = nodes[i]

                if (node.start-self.max_period <= curr_node.start <=
                    node.start-self.min_period):
                    node.add_input(curr_node)

            node.extend_chain()

    def find_chains(self, query):
        for c in query.candidates:
            self._find_candidate_chains(c)
