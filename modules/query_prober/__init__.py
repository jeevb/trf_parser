import regex

from formats.trf_record import parse
from modules.query import Query
from modules.chain_finder import ChainFinder
from modules.genome_aligner import GenomeAligner


class QueryProber():

    def __init__(self, config):
        self.config = config
        self.cf = ChainFinder(self.config.min_period, self.config.max_period)
        self.galign = GenomeAligner(self.config.genome, self.config.mismatches)

    def _probe_query(self, record, pattern):
        cpattern, mpattern = pattern
        cpattern = regex.compile(cpattern.format(word_len='%i,%i' % (
            self.config.min_word_length-1,
            self.config.max_word_length-1)))

        query = Query(record, cpattern, mpattern)

        query.find_candidates()
        if not query.candidates:
            return

        self.cf.find_chains(query)
        query.trim_candidates(self.config.min_tag_units)

        for c in query.candidates:
            self.galign.align_to_genome(c)
            record.add_candidate(c)

    def probed_queries(self, source, f):
        for record in parse(open(f, "rU")):
            record.source = source # Set the source of the TRF record
            for pattern in self.config.patterns:
                self._probe_query(record, pattern)
            if record.candidates:
                yield record
