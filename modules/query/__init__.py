import regex

from collections import defaultdict
from modules.node import Node


class Candidate():

    def __init__(self, query, whole, sgrna, var_bases):
        self.query = query

        self.whole = whole
        self.sgrna = sgrna
        self.var_bases = set(var_bases)

        self.nodes = []
        self.off_targets = defaultdict(set)

    def __hash__(self):
        return hash(self.sgrna)

    def __eq__(self, x):
        return self.sgrna == x

    def __ne__(self, x):
        return self.sgrna != x

    @property
    def score(self):
        return max(n.score for n in self.nodes) if self.nodes else 0

    def add_node(self, node):
        self.nodes.append(node)

    def sort_nodes(self):
        self.nodes.sort(key=lambda n: n.start)

    def add_off_target(self, bt):
        if self._is_valid_off_target(bt):
            self.off_targets[bt.chrom].add(bt.chrom_start)

    def _is_valid_off_target(self, bt):
        return self.var_bases.issuperset(bt.mismatches) and \
            (bt.chrom != self.query.record.source or
                not self.query.record.start_coord <= bt.chrom_start <=
                    self.query.record.end_coord)


class Query():

    def __init__(self, record, cpattern, mpattern):
        self.record = record
        self.cpattern = cpattern
        self.mpattern = mpattern

        self.candidates = set()

    @property
    def _is_adjacent_repeat(self):
        return self.record.period == self.record.consensus_size

    def _concat_seq(self):
        new_seq = self.record.consensus

        while not self._is_valid_seq(new_seq) and \
            self._is_adjacent_repeat and \
            len(new_seq) < 45:
            new_seq += self.record.consensus

        return new_seq

    def _is_valid_seq(self, seq):
        return self.cpattern.search(seq)

    def find_candidates(self):
        seq = self._concat_seq()
        for i in self.cpattern.finditer(seq):
            whole = i.group('whole')
            sgrna = i.group('sgrna')
            variable = i.group('variable')

            var_match = self.cpattern.match(whole)
            var_start = var_match.start('variable')
            var_end = var_match.end('variable')

            c = Candidate(self, whole, sgrna, range(var_start, var_end))
            self.candidates.add(c)

            for match in self._find_matches(variable):
                c.add_node(Node(match))

            c.sort_nodes()

    def _find_matches(self, variable):
        pattern = regex.compile(self.mpattern.format(variable=variable))
        for match in pattern.finditer(self.record.region):
            yield (match.start('whole'),
                   match.end('whole'))

    def trim_candidates(self, min_score):
        self.candidates = set(c for c in self.candidates
                              if c.score >= min_score)
