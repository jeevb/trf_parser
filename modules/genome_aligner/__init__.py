import subprocess

from formats.bowtie import BowtieRecord


class GenomeAligner():

    def __init__(self, ref, mismatches):
        self.ref = ref
        self.mismatches = mismatches

    def align_to_genome(self, candidate):
        bt_params = [
            "bowtie",
            "-c",
            "-v %i" % self.mismatches,
            "--all",
            self.ref,
            candidate.whole
        ]

        align = subprocess.Popen(
            bt_params,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )

        std_out, std_err = align.communicate()

        for result in std_out.splitlines():
            candidate.add_off_target(BowtieRecord(result.strip().split('\t')))
