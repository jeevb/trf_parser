class TRFRecord():

    def __init__(self, record):
        _record = record.strip().split()

        self.source = None
        self.start_coord = int(_record[0])
        self.end_coord = int(_record[1])
        self.period = int(_record[2])
        self.copies = float(_record[3])
        self.consensus_size = int(_record[4])
        self.percent_match = int(_record[5])
        self.indels_count = int(_record[6])
        self.score = int(_record[7])
        self.percent_a = int(_record[8])
        self.percent_c = int(_record[9])
        self.percent_g = int(_record[10])
        self.percent_t = int(_record[11])
        self.entropy = float(_record[12])
        self.consensus = _record[13].lower()
        self.region = _record[14].lower()

        self.candidates = set()

    def add_candidate(self, candidate):
        self.candidates.add(candidate)

    @property
    def candidate_str(self):
        return ','.join(
            '%s(%i|%i|%s)' % (
                c.whole,
                c.score,
                len(c.nodes),
                ','.join(
                    '%s:%i' % (
                        chrom,
                        len(idx_set)
                    ) for chrom, idx_set in c.off_targets.iteritems())
            ) for c in self.candidates)

    @property
    def summary(self):
        return '%i\t%i\t%i\t%0.3f\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%0.3f\t' \
            '%s\t%s\t%s\n' % (
                self.start_coord,
                self.end_coord,
                self.period,
                self.copies,
                self.consensus_size,
                self.percent_match,
                self.indels_count,
                self.score,
                self.percent_a,
                self.percent_c,
                self.percent_g,
                self.percent_t,
                self.entropy,
                self.consensus,
                self.candidate_str,
                self.region)


def parse(handler):
    for record in handler.xreadlines():
        yield TRFRecord(record)
