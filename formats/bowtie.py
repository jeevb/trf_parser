import regex

MISMATCH_REGEX = regex.compile(
    r'(?P<position>\d+)\:(?P<original>\w)>(?P<aligned>\w)')


class BowtieRecord():

    def __init__(self, record):
        self.chrom = record[2]
        self.seq = record[4].lower()
        self.chrom_start = int(record[3])
        self.chrom_end = self.chrom_start + len(self.seq)

        self.mismatches = set()
        try:
            self._parse_mismatches(record[7])
        except IndexError:
            pass

    def _parse_mismatches(self, mismatches):
        _mismatches = mismatches.split(',')
        for i in _mismatches:
            self.mismatches.add(MISMATCH_REGEX.search(i).group('position'))
