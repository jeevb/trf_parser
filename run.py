import os
import regex

from glob import glob
from formats.trf_record import parse
from modules.query_prober import QueryProber
from settings import Settings as config

def main(f):
    source = regex.search(
        r'.*/(?P<source>.+)\.txt', f).group('source').strip('_')

    Q = QueryProber(config)

    path_to_output = os.path.join(config.output_path,
                                  '%s_filtered.txt' % source)
    with open(path_to_output, "w") as output:
        for record in Q.probed_queries(source, f):
            output.write(record.summary)

def wrapper():
    if not os.path.exists(config.output_path):
        os.makedirs(config.output_path)

    for f in glob(config.path_to_files + '*.txt'):
        print "Processing records file %s..." % f
        main(f)

if __name__ == "__main__":
    wrapper()
