class Settings():

    # Path to the files containing the TRF records within which
    # CRISPR targets will need to be identified
    path_to_files = "/home/sba/bioinf/juan/genome_candidates/"

    # Path to write output files to
    output_path = "/home/sba/bioinf/juan/genome_candidates_filtered/"

    # Path to genome bowtie index
    genome = "/home/sba/bioinf/hg19/hg19"

    # Max number of mismatches when searching for candidate matches in
    # repeat regions
    mismatches = 2

    # Bounds for sequence lengths
    min_word_length = 15
    max_word_length = 30

    # Specify the minimum and maximum sizes of periods used when
    # building a region to tag
    min_period = 30
    max_period = 2000

    # Only consider regions that have at least min_tag_units tagging units
    min_tag_units = 10

    ###########################################################################
    # DO NOT CHANGE THESE                                                     #
    ###########################################################################
    # Patterns for extraction of CRISPR targets
    # Typical CRISPR targets will have one of the following patterns:
    # (1) g(n)xngg
    # (2) ccn(n)xc
    patterns = [
        # g(n)xngg
        (
            r'(?=(?P<whole>(?P<sgrna>g(?P<variable>.{{{word_len}}})).gg))',
            r'(?=(?P<whole>g((?:{variable}){{s<=%i}}).gg))' % mismatches
        ),
        # ccn(n)xc
        (
            r'(?=(?P<whole>cc.(?P<sgrna>(?P<variable>.{{{word_len}}})c)))',
            r'(?=(?P<whole>cc.((?:{variable}){{s<=%i}})c))' % mismatches
        )
    ]
    ###########################################################################
